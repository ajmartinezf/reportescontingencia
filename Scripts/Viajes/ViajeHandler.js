﻿'use strict';
var ViajeHandler = function () {
    let dpini = $('#datetimepickerini');
    let dpfin = $('#datetimepickerfin');
    let cedis = $('#controlselect1');
    let boton = $('#btnConsultar');
    var table = $('#kt_table_1');

    var initDP = function () {
        dpini.datetimepicker({ format: 'DD/MM/YYYY' });
        dpfin.datetimepicker({ format: 'DD/MM/YYYY' });
    };

    var componentsHandler = function () {



        boton.click(function () {
            //let t = dpini.data('date');
            table.DataTable().destroy();
            initTable1();

            //alert(valorete);
            //alert("The paragraph was clicked.");
        });
    };

    var obtenerBranches = function () {
        
        $.ajax({
            url: getbranches,
            type:"GET",
            success: function (respuesta) {
                //debugger;
                var listaUsuarios = $("#lista-usuarios");
                $.each(respuesta.data, function (index, elemento) {

                    debugger;
                    cedis.append('<option value="' + elemento.puntoVta + '">' + '( ' + elemento.puntoVta + ' ) ' +  elemento.cedis + '</option>');
                });
                cedis.multiselect({
                    includeSelectAllOption: true,
                    nonSelectedText: 'Selecciona algunos cedis',
                    maxHeight: 350,
                    enableFiltering: true
                });
            },
            error: function () {
                console.log("No se ha podido obtener la información");
                alert("No se pudo obtener la información de los cedis");
            }
        });
    };

    var initTable1 = function () {
        var cedisselected = cedis.children("option:selected").val();
        var cedises = [];
        $("#controlselect1 > option:selected").each(function () {
            cedises.push(this.value);
        });
        var str = cedises.join(',');
        //alert(str);
        var _uri = branchpath + "?branc=" + encodeURIComponent(str);
        //alert(_uri);
        table.DataTable({
            responsive: true,
            searching: false,
            pageLength: 50,
            dom: 'Bfrtip',
            buttons: [
                {
                    
                extend: 'csvHtml5',
                    text: 'Descargar'
                }
                
            ],
            language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla =(",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "infoPostFix": "",
                "search": "Buscar:",
                "url": "",
                "infoThousands": ",",
                "csv": "descargar",
                "loadingRecords": "Cargando...",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
            ajax: {

                url: _uri,
                type: 'GET'
            },
            columns: [
                { data: 'fila' },
                { data: 'puntoVta' },
                { data: 'cedis' },
                { data: 'viajes_programados' },
                { data: 'viajes_iniciados' },
                { data: 'viajes_terminados' },
                { data: 'progreso' }
            ],
            columnDefs: [
                {
                    "targets": 0,
                    "visible": false,
                },
                {
                    targets: 6,
                    render: function (data) {
                        let porcentaje = '<div class="progress">' +
                            '<div class="progress-bar" role = "progressbar" aria-valuenow="' + data + '" aria-valuemin="0" aria-valuemax="100" style = "width:' + data +'%;  color:black;" >' +
                            data + '%'+
                            ' </div >' +
                            '</div >';
                        return porcentaje;
                    }
                }
            ]
        });
    };

    return {

        //main function to initiate the module
        init: function () {
            obtenerBranches();
            initDP();
            componentsHandler();
            //initTable1();
        }

    };

}();

jQuery(document).ready(function () {
    ViajeHandler.init();
});