﻿'use strict';
var OrderSumHandler = function () {
    let dpini = $('#datetimepickerini');
    let dpfin = $('#datetimepickerfin');
    let cedis = $('#controlselect1');
    let usuario = $('#controlselect2');
    let boton = $('#btnConsultar');


    var initDP = function () {
        dpini.datetimepicker({ format: 'DD/MM/YYYY'});
        dpfin.datetimepicker({ format: 'DD/MM/YYYY'});
    };

    var componentsHandler = function () {
        boton.click(function () {
            let t = dpini.data('date');
            alert(t);
            //alert("The paragraph was clicked.");
        });


    };

    var obtenerBranches = function () {
        $.ajax({
            url: 'https://reqres.in/api/users',
            success: function (respuesta) {

                var listaUsuarios = $("#lista-usuarios");
                $.each(respuesta.data, function (index, elemento) {
                    listaUsuarios.append(
                        '<div>'
                        + '<p>' + elemento.first_name + ' ' + elemento.last_name + '</p>'
                        + '<img src=' + elemento.avatar + '></img>'
                        + '</div>'
                    );
                });
            },
            error: function () {
                console.log("No se ha podido obtener la información");
            }
        });
    
    }

    var initTable1 = function () {
        var table = $('#kt_table_1');
        var _uri = branchpath + "?branc=" + "31";
        alert(_uri);
        // begin first table
        table.DataTable({
            responsive: true,
            ajax: {

                url: _uri,
                type: 'GET'
                //data: {
                //    pagination: {
                //        perpage: 50,
                //    }
                //}
            },
            columns: [
                { data: 'puntoVta' },
                { data: 'cedis' },
                { data: 'viajes' },
                { data: 'viajes_iniciados' },
                { data: 'viajes_terminados' }
            ]
            //columnDefs: [
            //    {
            //        targets: -1,
            //        title: 'Actions',
            //        orderable: false,
            //        render: function (data, type, full, meta) {
            //            return `
            //            <span class="dropdown">
            //                <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
            //                  <i class="la la-ellipsis-h"></i>
            //                </a>
            //                <div class="dropdown-menu dropdown-menu-right">
            //                    <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
            //                    <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>
            //                    <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
            //                </div>
            //            </span>
            //            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
            //              <i class="la la-edit"></i>
            //            </a>`;
            //        },
            //    },
            //    {
            //        targets: -3,
            //        render: function (data, type, full, meta) {
            //            var status = {
            //                1: { 'title': 'Pending', 'class': 'kt-badge--brand' },
            //                2: { 'title': 'Delivered', 'class': ' kt-badge--danger' },
            //                3: { 'title': 'Canceled', 'class': ' kt-badge--primary' },
            //                4: { 'title': 'Success', 'class': ' kt-badge--success' },
            //                5: { 'title': 'Info', 'class': ' kt-badge--info' },
            //                6: { 'title': 'Danger', 'class': ' kt-badge--danger' },
            //                7: { 'title': 'Warning', 'class': ' kt-badge--warning' },
            //            };
            //            if (typeof status[data] === 'undefined') {
            //                return data;
            //            }
            //            return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>';
            //        },
            //    },
            //    {
            //        targets: -2,
            //        render: function (data, type, full, meta) {
            //            var status = {
            //                1: { 'title': 'Online', 'state': 'danger' },
            //                2: { 'title': 'Retail', 'state': 'primary' },
            //                3: { 'title': 'Direct', 'state': 'success' },
            //            };
            //            if (typeof status[data] === 'undefined') {
            //                return data;
            //            }
            //            return '<span class="kt-badge kt-badge--' + status[data].state + ' kt-badge--dot"></span>&nbsp;' +
            //                '<span class="kt-font-bold kt-font-' + status[data].state + '">' + status[data].title + '</span>';
            //        },
            //    },
            //]
        });
    };

    return {

        //main function to initiate the module
        init: function () {
            initDP();
            componentsHandler();
            initTable1();
        }

    };

}();

jQuery(document).ready(function () {
    OrderSumHandler.init();
});